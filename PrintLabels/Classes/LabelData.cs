﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintLabels.Classes
{
    public class LabelData
    {
        private string openingPromoText;
        private double percentOff;
        private string secondaryPromoText;
        private bool includeLine;
        private bool includeProducts;
        private List<string> listProducts;
        private string promoCode;
        private string website;
        private string productName;
        private string productDescription;
        private string qrCode;
        private string qrCodeInstructions;
        private string reorderInstructions;

        private string size_productName;
        private string size_productDescription;
        private string lines_productDescription;

        public string OpeningPromoText
        {
            get { return openingPromoText; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    openingPromoText = "Save Now";
                }
                else
                {
                    openingPromoText = MakeUpper(value);
                }
            }
        }

        public double PercentOff
        {
            get { return percentOff; }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    percentOff = value;
                }
                else
                {
                    percentOff = 20.0;
                }
            }
        }

        public string SecondaryPromoText
        {
            get { return secondaryPromoText; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    secondaryPromoText = "On all filters for your home";
                }
                else
                {
                    secondaryPromoText = MakeUpper(value);
                }
            }
        }

        public bool IncludeLine
        {
            get { return includeLine; }
            set { includeLine = value; }
        }

        public bool IncludeProducts
        {
            get { return includeProducts; }
            set { includeProducts = value; }
        }

        public List<string> ListProducts
        {
            get { return listProducts; }
            set
            {
                if (listProducts == null)
                {
                    listProducts = new List<string>();

                    if (value != null)
                    {
                        if (value.Count == 1)
                        {
                            listProducts = value;
                        }
                        else if (value.Count > 1)
                        {
                            foreach (string product in value)
                            {
                                listProducts.Add(MakeUpper(product));
                            }
                        }
                    }
                    else
                    {
                        for (int x = listProducts.Count; x < 10; x++)
                        {
                            listProducts.Add("");
                        }
                    }
                }
            }
        }

        public string PromoCode
        {
            get { return promoCode; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    promoCode = "RETURN20";
                }
                else
                {
                    promoCode = MakeUpper(value);
                }
            }
        }

        public string Website
        {
            get { return website; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    website = "DiscountFilters.com";
                }
                else
                {
                    website = value;
                }
            }
        }

        public string ProductName
        {
            get { return productName; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    productName = "";
                }
                else if (value.Length > 27)
                {
                    //productName = value.Substring(0, 20);
                    productName = value;
                    Size_productName = "S";
                }
                else if (value.Length > 20)
                {
                    //productName = value.Substring(0, 20);
                    productName = value;
                    Size_productName = "U";
                }
                else
                {
                    productName = MakeUpper(value);
                }
            }
        }

        public string ProductDescription
        {
            get { return productDescription; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    productDescription = "";
                }
                else if (value.Length > 60)
                {
                    productDescription = MakeUpper(value);
                    Size_productDescription = "Q";
                    lines_productDescription = "6";
                }
                else if (value.Length > 45)
                {
                    productDescription = MakeUpper(value);
                    Size_productDescription = "S";
                    lines_productDescription = "4";
                }
                else
                {
                    productDescription = MakeUpper(value);
                }
            }
        }

        public string QRCode
        {
            get { return qrCode; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    qrCode = "www.DiscountFilters.com";
                }
                else
                {
                    qrCode = MakeUpper(value);
                }
            }
        }

        public string QRCodeInstructions
        {
            get { return qrCodeInstructions; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    qrCodeInstructions = "Scan to reorder";
                }
                else
                {
                    qrCodeInstructions = MakeUpper(value);
                }
            }
        }

        public string ReorderInstructions
        {
            get { return reorderInstructions; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    reorderInstructions = "For more information, reordering or installation instructions, visit us at www.DiscountFilters.com or scan the code.";
                }
                else
                {
                    reorderInstructions = value;
                }
            }
        }

        public string Size_productName
        {
            get { return size_productName; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    size_productName = value;
                }
            }
        }

        public string Size_productDescription
        {
            get { return size_productDescription; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    size_productDescription = value;
                }
            }
        }

        public string Lines_productDescription
        {
            get { return lines_productDescription; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    lines_productDescription = value;
                }
            }
        }

        private string MakeUpper(string originalText)
        {
            string newText = "";

            try
            {
                if (!String.IsNullOrEmpty(originalText))
                {
                    newText = originalText.ToUpper();
                }
            }
            catch (Exception ex)
            {

            }

            return newText;
        }

    }
}
