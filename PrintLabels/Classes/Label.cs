﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintLabels.Print;

namespace PrintLabels.Classes
{
    public class Label
    {
        public LabelData data { get; set; }
        public string zpl { get; set; }

        public bool continuous { get; set; }
        public int labelLength { get; set; }

        private string continuousCode { get; set; }
        private string labelLengthCode { get; set; }

        public Label(LabelData newData)
        {
            data = newData;
        }

        public void Print()
        {
            zpl = GenerateZPL();

            Print.Print print = new Print.Print();

            //Prints the Promo Label
            print.ZPLLabel(zpl);
        }

        private string GenerateZPL()
        {
            if (continuous)
            {
                continuousCode = "^MNN";
                labelLengthCode = "^LL" + labelLength;
            }
            else
            {
                continuousCode = "^MNY";
                labelLengthCode = "";
            }

            string dynamicZPL = $@"
                            ^XA
            {continuousCode}
            {labelLengthCode}
            ^XGMyFormat^FS
                            ^FX Opening Text
                            ^CF0,100, 100
                            ^FO0,100^FB812,1,0,C^FD{data.OpeningPromoText}^FS  

                            ^FX Percent Off
                            ^CFR,200,200
                            ^FO0,170^FB812,1,0,C^FD{data.PercentOff}% OFF^FS

                            ^FX Secondary Text
                            ^CF00,90
                            ^FO50, 350^FB712,2,0,C^FD{data.SecondaryPromoText}^FS

                            ^FX Line
                            ^FO50,550^GB712,0,4,^FS

                            ^FX Products, First Column
                            ^CFS
                            ^FO50, 575^FB366,2,0,C^FD{data.ListProducts[0]}^FS
                            ^FO50, 615^FB366,2,0,C^FD{data.ListProducts[1]}^FS
                            ^FO50, 655^FB366,2,0,C^FD{data.ListProducts[2]}^FS
                            ^FO50, 695^FB366,2,0,C^FD{data.ListProducts[3]}^FS
                            ^FO50, 735^FB366,2,0,C^FD{data.ListProducts[4]}^FS
            ^FO50, 775^FB366,2,0,C^FD{data.ListProducts[10]}^FS
            ^FO50, 815^FB366,2,0,C^FD{data.ListProducts[11]}^FS

                            ^FX Products, Second Column
                            ^CFS
                            ^FO416, 575^FB366,2,0,C^FD{data.ListProducts[5]}^FS
                            ^FO416, 615^FB366,2,0,C^FD{data.ListProducts[6]}^FS
                            ^FO416, 655^FB366,2,0,C^FD{data.ListProducts[7]}^FS
                            ^FO416, 695^FB366,2,0,C^FD{data.ListProducts[8]}^FS
                            ^FO416, 735^FB366,2,0,C^FD{data.ListProducts[9]}^FS
            ^FO416, 775^FB366,2,0,C^FD{data.ListProducts[12]}^FS
            ^FO416, 775^FB366,2,0,C^FD{data.ListProducts[13]}^FS

                            ^FX DiscountCode
                            ^CFV,60,60
                            ^FO0,875^FB812,1,0,C^FDUse code: {data.PromoCode} at^FS
                            ^FO0,935^FB812,1,0,C^FD{data.Website}^FS

                            ^FX Product Name
                            ^CFV,0,10
                            ^FO50,1100^FD{data.ProductName}^FS

                            ^FX Product Description
                            ^CFR,0,1
                            ^FO50,1180^FB366,3,0,L^FD{data.ProductDescription}^FS

                            ^FX Reorder Instructions
                            ^CFR,0,5
                            ^FO50,1350^FB366,6,0,L^FD{data.ReorderInstructions}^FS

                            ^FX QR Instructions
                            ^CFS,0,40
                            ^FO500,1200^FB250,4,0,C^FD{data.QRCodeInstructions}^FS

                            ^FX QR Code
                            ^FO500,1275
                            ^BQN,2,10
                            ^FDMM,A{data.QRCode}^FS

                            ^FX URL
                            ^FX QR Instructions
                            ^CFR,0,14
                            ^FO50,1550^FB712,4,0,R^FD{data.QRCode}^FS


                            ^XZ
                            ";

            // Bottom label only
            //string dynamicZPL = $@"
            //    ^XA

            //    ^FX Product Name
            //    ^CF{data.Size_productName}
            //    ^FO50,100^FD{data.ProductName}^FS
            //    ^CF{data.Size_productDescription}
            //    ^FO50,180^FB366,{data.Lines_productDescription},0,L^FD{data.ProductDescription}^FS

            //    ^FX Reorder Instructions
            //    ^CFR,0,3
            //    ^FO50,350^FB366,5,0,L^FD{data.ReorderInstructions}^FS

            //    ^FX QR Instructions
            //    ^CFS,0,40
            //    ^FO500,200^FB250,4,0,C^FD{data.QRCodeInstructions}^FS

            //    ^FX QR Code
            //    ^FO500,100
            //    ^BQN,2,10
            //    ^FDMM,A{data.QRCode}^FS

            //    ^FX URL
            //    ^FX QR Instructions
            //    ^CFR,0,14
            //    ^FO50,550^FB712,4,0,R^FD{data.QRCode}^FS


            //    ^XZ
            //    ";

            return dynamicZPL;
        }
    }
}
