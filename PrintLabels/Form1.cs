﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrintLabels.Classes;
using Label = PrintLabels.Classes.Label;

namespace PrintLabels
{
    public partial class frmPromoLabels : Form
    {
        public frmPromoLabels()
        {
            InitializeComponent();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            LabelData labelInfo = new LabelData
            {
                OpeningPromoText = txtOpeningPromo.Text,
                PercentOff = int.Parse(txtPercentOff.Text),
                SecondaryPromoText = txtSecondaryPromoText.Text,
                PromoCode = txtPromoCode.Text,
                Website = txtWebsite.Text,
                ProductName = txtProductName.Text,
                ProductDescription = txtProductDescription.Text,
                QRCode = txtQRCodeAddress.Text,
                QRCodeInstructions = txtQRCodeInstructions.Text,
                ReorderInstructions = txtReorderInstructions.Text,
            };

            labelInfo.ListProducts = new List<string>();
            labelInfo.ListProducts.Add(txtProduct1.Text);
            labelInfo.ListProducts.Add(txtProduct2.Text);
            labelInfo.ListProducts.Add(txtProduct3.Text);
            labelInfo.ListProducts.Add(txtProduct4.Text);
            labelInfo.ListProducts.Add(txtProduct5.Text);
            labelInfo.ListProducts.Add(txtProduct6.Text);
            labelInfo.ListProducts.Add(txtProduct7.Text);
            labelInfo.ListProducts.Add(txtProduct8.Text);
            labelInfo.ListProducts.Add(txtProduct9.Text);
            labelInfo.ListProducts.Add(txtProduct10.Text);
            labelInfo.ListProducts.Add(txtProduct11.Text);
            labelInfo.ListProducts.Add(txtProduct12.Text);
            labelInfo.ListProducts.Add(txtProduct13.Text);
            labelInfo.ListProducts.Add(txtProduct14.Text);

            Label label = new Label(labelInfo);
            label.continuous = chkContinuous.Checked;
            if (txtLabelLength.Enabled)
            {
                double dblLabelLength = (double.Parse(txtLabelLength.Text) * 203.2);
                label.labelLength = (int)Math.Ceiling(dblLabelLength);
            }
            else
            {
                label.labelLength = 0;
            }
            label.Print();
        }

        private void changeTextColorToBlack(TextBox txtBox)
        {
            txtBox.ForeColor = Color.Black;
            chkUseDefaults.Checked = false;
            chkUseDefaults.Enabled = true;
        }

        private void changeTextColorToGray(TextBox txtBox)
        {
            txtBox.ForeColor = SystemColors.AppWorkspace;
            chkUseDefaults.Checked = true;
        }

        private void txtOpeningPromo_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtPercentOff_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtSecondaryPromoText_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct1_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct2_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct3_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct4_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct5_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct6_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct7_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct8_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct9_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct10_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct11_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct12_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct13_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProduct14_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtPromoCode_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtWebsite_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProductName_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtProductDescription_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtQRCodeAddress_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtQRCodeInstructions_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtReorderInstructions_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void txtLabelLength_TextChanged(object sender, EventArgs e)
        {
            changeTextColorToBlack((TextBox)sender);
        }

        private void chkContinuous_CheckedChanged(object sender, EventArgs e)
        {
            if (chkContinuous.Checked)
            {
                txtLabelLength.Enabled = true;
            }
            else
            {
                txtLabelLength.Enabled = false;
            }
        }

        private void chkUseDefaults_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUseDefaults.Checked)
            {
                this.txtOpeningPromo.Text = "Save Now";
                this.txtPercentOff.Text = "20";
                this.txtSecondaryPromoText.Text = "On all filters for your home";
                this.txtProduct1.Text = "FRIDGE FILTERS";
                this.txtProduct2.Text = "HUMIDIFIER FILTERS";
                this.txtProduct4.Text = "AIR CLEANERS";
                this.txtProduct3.Text = "POOL & SPA FILTERS";
                this.txtProduct6.Text = "UNDER SINK";
                this.txtProduct5.Text = "SEDIMENT FILTERS";
                this.txtProduct8.Text = "HUMIDIFIER";
                this.txtProduct7.Text = "WHOLE HOUSE";
                this.txtProduct9.Text = "CLEANING SUPPLIES";
                this.txtProduct10.Text = "UV BULBS";
                this.txtProduct12.Text = "FURNACE FILTERS";
                this.txtProduct11.Text = "AC FILTERS";
                this.txtProduct13.Text = "CARBON FILTERS";
                this.txtProduct14.Text = "";
                this.txtPromoCode.Text = "RETURN20";
                this.txtWebsite.Text = "DiscountFilters.com";
                this.txtProductName.Text = "WATER FILTER";
                this.txtProductDescription.Text = "High quality water filter.";
                this.txtQRCodeAddress.Text = "www.discountfilters.com/PS01-12345";
                this.txtReorderInstructions.Text = "For more information, reordering or installation instructions, visit us at www.Di" +
                                                   "scountFitlers.com or scan the code.";
                this.txtQRCodeInstructions.Text = "Scan to reorder";
                chkContinuous.Checked = true;
                this.txtLabelLength.Text = "12.25";

                foreach (Control c in this.Controls)
                {
                    if (c.GetType().ToString() == "System.Windows.Forms.TextBox")
                    {
                        changeTextColorToGray((TextBox)c);
                    }
                }

                chkUseDefaults.Enabled = false;
            }
        }


    }
}
