﻿using System.Runtime.InteropServices;
using System;
using System.IO;

namespace PrintLabels.Print
{
	/// <summary>
	/// Based on Microsoft code to send raw data directly to a printer.
	/// See http://support.microsoft.com/kb/322091
	/// </summary>
	public class RawDataPrinterHelper
	{
		// Structure and API declarations:
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		public struct DOCINFOW
		{
			[MarshalAs(UnmanagedType.LPWStr)]
			public string pDocName;
			[MarshalAs(UnmanagedType.LPWStr)]
			public string pOutputFile;
			[MarshalAs(UnmanagedType.LPWStr)]
			public string pDataType;
		}

		[DllImport("winspool.Drv", EntryPoint = "OpenPrinterW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		private static extern bool OpenPrinter(string src, ref IntPtr hPrinter, IntPtr pd);
		[DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		private static extern bool ClosePrinter(IntPtr hPrinter);
		[DllImport("winspool.Drv", EntryPoint = "StartDocPrinterW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		private static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, ref DOCINFOW pDI);
		[DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		private static extern bool EndDocPrinter(IntPtr hPrinter);
		[DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		private static extern bool StartPagePrinter(IntPtr hPrinter);
		[DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		private static extern bool EndPagePrinter(IntPtr hPrinter);
		[DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		private static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, ref Int32 dwWritten);

		/// <summary>
		/// Sends an unmanaged array of bytes to the specified printer queue.
		/// </summary>
		/// <param name="printerName">The name of the printer.</param>
		/// <param name="byteCount"></param>
		/// <param name="dwCount"></param>
		/// <returns>true on success, false on failure.</returns>
		public static bool SendBytesToPrinter(string printerName, IntPtr byteCount, Int32 dwCount)
		{
			IntPtr   hPrinter  = default(IntPtr);      // The printer handle.
			Int32    dwError   = default(Int32);       // Last error - in case there was trouble.
			DOCINFOW di        = default(DOCINFOW);    // Describes your document (name, port, data type).
			Int32    dwWritten = default(Int32);       // The number of bytes written by WritePrinter().
			bool     success   = false;                // Your success code.

			// Set up the DOCINFO structure.
			di.pDocName  = "NT Shipping Label";
			di.pDataType = "RAW";

#pragma warning disable 168
// ReSharper disable EmptyGeneralCatchClause
			try
			{
				success = OpenPrinter(printerName.Normalize(), ref hPrinter, (IntPtr)0);
			}
			catch (Exception ex)
			{
			}
// ReSharper restore EmptyGeneralCatchClause
#pragma warning restore 168

			if (success)
			{
				if (StartDocPrinter(hPrinter, 1, ref di))
				{
					if (StartPagePrinter(hPrinter))
					{
						// Write your printer-specific bytes to the printer.
						success = WritePrinter(hPrinter, byteCount, dwCount, ref dwWritten);
						EndPagePrinter(hPrinter);
					}
					EndDocPrinter(hPrinter);
				}
				ClosePrinter(hPrinter);
			}

			// If you did not succeed, GetLastError may give more information about why not.
			if (success == false)
				dwError = Marshal.GetLastWin32Error();

			return success;
		}

		/// <summary>
		///		Sends the contents of a file to the specified printer queue.
		///		Presumes that the file contains printer-ready data.
		///		Shows how to use the SendBytesToPrinter function.
		/// </summary>
		/// <param name="printerName">The name of the printer.</param>
		/// <param name="fileName">The full path.</param>
		/// <returns>true on success, false on failure.</returns>
		public static bool SendFileToPrinter(string printerName, string fileName)
		{
			// Open the file, and create a BinaryReader.
			bool success;
			IntPtr pUnmanagedBytes;
			using (FileStream fs = new FileStream(fileName, FileMode.Open))
			{
				BinaryReader br = new BinaryReader(fs);
				byte[] bytes = new byte[fs.Length + 1];
				success = false;
				// Your unmanaged pointer.
				pUnmanagedBytes = default(IntPtr);

				// Read the contents of the file into the array.
				bytes = br.ReadBytes((int)fs.Length);
				// Allocate some unmanaged memory for those bytes.
				pUnmanagedBytes = Marshal.AllocCoTaskMem((int)fs.Length);
				// Copy the managed byte array into the unmanaged array.
				Marshal.Copy(bytes, 0, pUnmanagedBytes, (int)fs.Length);
				// Send the unmanaged bytes to the printer.
				success = SendBytesToPrinter(printerName, pUnmanagedBytes, (int)fs.Length);
			}
			// Free the unmanaged memory that you allocated earlier.
			Marshal.FreeCoTaskMem(pUnmanagedBytes);
			return success;
		}

		/// <summary>
		/// When the function is given a string and a printer name, the function sends the string to the printer as raw bytes.
		/// </summary>
		/// <param name="printerName">The name of the printer.</param>
		/// <param name="text">The string to be sent to the printer.</param>
		/// <returns>true on success, false on failure.</returns>
		public static bool SendStringToPrinter(string printerName, string text)
		{
			IntPtr pBytes = default(IntPtr);
			Int32 dwCount = default(Int32);
			bool success;

			// How many characters are in the string?
			dwCount = text.Length;
			// Assume that the printer is expecting ANSI text, and then convert the string to ANSI text.
			pBytes = Marshal.StringToCoTaskMemAnsi(text);

			// Send the converted ANSI string to the printer.
			success = SendBytesToPrinter(printerName, pBytes, dwCount);
			Marshal.FreeCoTaskMem(pBytes);

			return success;
		}
	}
}
