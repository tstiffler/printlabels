﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintLabels.Print
{
    public class Print
    {
        public string PrinterName { get; set; }
        public int LeftOffset { get; set; }
        public int TopOffset { get; set; }

        public Print()
        {
            PrinterName = PrintLabels.Properties.Settings.Default.PrinterName;
            LeftOffset = PrintLabels.Properties.Settings.Default.LeftOffset;
            TopOffset = PrintLabels.Properties.Settings.Default.TopOffset;
        }

        public void ZPLLabel(string ZPLToPrint)
        {
            if (TopOffset != 0 ||LeftOffset != 0)
            {
                try
                {
                    int indexOfLabelHeader = ZPLToPrint.IndexOf("^LH");
                    int indexOfNextTag = ZPLToPrint.IndexOf("^", indexOfLabelHeader + 1, ZPLToPrint.Length - (indexOfLabelHeader + 1));
                    string labelHeaderTag = ZPLToPrint.Substring(indexOfLabelHeader, (indexOfNextTag - indexOfLabelHeader)).Trim();

                    ZPLToPrint = ZPLToPrint.Replace(labelHeaderTag, string.Format("^LH{0},{1}", LeftOffset, TopOffset));
                }
                catch (Exception ex)
                {
                    //Do Nothing
                }
            }

            //Sends the ZPL to the Given Printer
            RawDataPrinterHelper.SendStringToPrinter(PrinterName, ZPLToPrint);
        }
    }
}
