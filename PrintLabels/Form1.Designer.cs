﻿namespace PrintLabels
{
    partial class frmPromoLabels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPromoLabels));
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtOpeningPromo = new System.Windows.Forms.TextBox();
            this.lblOpeningPromoText = new System.Windows.Forms.Label();
            this.lblPercentOff = new System.Windows.Forms.Label();
            this.txtPercentOff = new System.Windows.Forms.TextBox();
            this.lblSecondaryPromoText = new System.Windows.Forms.Label();
            this.txtSecondaryPromoText = new System.Windows.Forms.TextBox();
            this.lblProducts = new System.Windows.Forms.Label();
            this.txtProduct1 = new System.Windows.Forms.TextBox();
            this.txtProduct2 = new System.Windows.Forms.TextBox();
            this.txtProduct4 = new System.Windows.Forms.TextBox();
            this.txtProduct3 = new System.Windows.Forms.TextBox();
            this.txtProduct6 = new System.Windows.Forms.TextBox();
            this.txtProduct5 = new System.Windows.Forms.TextBox();
            this.txtProduct8 = new System.Windows.Forms.TextBox();
            this.txtProduct7 = new System.Windows.Forms.TextBox();
            this.txtProduct9 = new System.Windows.Forms.TextBox();
            this.txtProduct10 = new System.Windows.Forms.TextBox();
            this.txtProduct12 = new System.Windows.Forms.TextBox();
            this.txtProduct11 = new System.Windows.Forms.TextBox();
            this.txtProduct14 = new System.Windows.Forms.TextBox();
            this.txtProduct13 = new System.Windows.Forms.TextBox();
            this.lblPromoCode = new System.Windows.Forms.Label();
            this.txtPromoCode = new System.Windows.Forms.TextBox();
            this.lblWebsite = new System.Windows.Forms.Label();
            this.txtWebsite = new System.Windows.Forms.TextBox();
            this.lblProductName = new System.Windows.Forms.Label();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.lblProductDescription = new System.Windows.Forms.Label();
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.lblQRCodeAddress = new System.Windows.Forms.Label();
            this.txtQRCodeAddress = new System.Windows.Forms.TextBox();
            this.lblReorderInstructions = new System.Windows.Forms.Label();
            this.txtReorderInstructions = new System.Windows.Forms.TextBox();
            this.lblQRCodeInstructions = new System.Windows.Forms.Label();
            this.txtQRCodeInstructions = new System.Windows.Forms.TextBox();
            this.chkContinuous = new System.Windows.Forms.CheckBox();
            this.lblContinuous = new System.Windows.Forms.Label();
            this.lblLabelLength = new System.Windows.Forms.Label();
            this.txtLabelLength = new System.Windows.Forms.TextBox();
            this.chkUseDefaults = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.Control;
            this.btnPrint.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnPrint.FlatAppearance.BorderSize = 2;
            this.btnPrint.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(708, 700);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(80, 29);
            this.btnPrint.TabIndex = 27;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtOpeningPromo
            // 
            this.txtOpeningPromo.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtOpeningPromo.Location = new System.Drawing.Point(134, 15);
            this.txtOpeningPromo.Name = "txtOpeningPromo";
            this.txtOpeningPromo.Size = new System.Drawing.Size(293, 20);
            this.txtOpeningPromo.TabIndex = 1;
            this.txtOpeningPromo.Text = "Save Now";
            this.txtOpeningPromo.TextChanged += new System.EventHandler(this.txtOpeningPromo_TextChanged);
            // 
            // lblOpeningPromoText
            // 
            this.lblOpeningPromoText.AutoSize = true;
            this.lblOpeningPromoText.Location = new System.Drawing.Point(12, 18);
            this.lblOpeningPromoText.Name = "lblOpeningPromoText";
            this.lblOpeningPromoText.Size = new System.Drawing.Size(107, 13);
            this.lblOpeningPromoText.TabIndex = 2;
            this.lblOpeningPromoText.Text = "Opening Promo Text:";
            // 
            // lblPercentOff
            // 
            this.lblPercentOff.AutoSize = true;
            this.lblPercentOff.Location = new System.Drawing.Point(12, 56);
            this.lblPercentOff.Name = "lblPercentOff";
            this.lblPercentOff.Size = new System.Drawing.Size(64, 13);
            this.lblPercentOff.TabIndex = 4;
            this.lblPercentOff.Text = "Percent Off:";
            // 
            // txtPercentOff
            // 
            this.txtPercentOff.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtPercentOff.Location = new System.Drawing.Point(134, 53);
            this.txtPercentOff.Name = "txtPercentOff";
            this.txtPercentOff.Size = new System.Drawing.Size(293, 20);
            this.txtPercentOff.TabIndex = 2;
            this.txtPercentOff.Text = "20";
            this.txtPercentOff.TextChanged += new System.EventHandler(this.txtPercentOff_TextChanged);
            // 
            // lblSecondaryPromoText
            // 
            this.lblSecondaryPromoText.AutoSize = true;
            this.lblSecondaryPromoText.Location = new System.Drawing.Point(12, 94);
            this.lblSecondaryPromoText.Name = "lblSecondaryPromoText";
            this.lblSecondaryPromoText.Size = new System.Drawing.Size(118, 13);
            this.lblSecondaryPromoText.TabIndex = 6;
            this.lblSecondaryPromoText.Text = "Secondary Promo Text:";
            // 
            // txtSecondaryPromoText
            // 
            this.txtSecondaryPromoText.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtSecondaryPromoText.Location = new System.Drawing.Point(134, 91);
            this.txtSecondaryPromoText.Name = "txtSecondaryPromoText";
            this.txtSecondaryPromoText.Size = new System.Drawing.Size(293, 20);
            this.txtSecondaryPromoText.TabIndex = 3;
            this.txtSecondaryPromoText.Text = "On all filters for your home";
            this.txtSecondaryPromoText.TextChanged += new System.EventHandler(this.txtSecondaryPromoText_TextChanged);
            // 
            // lblProducts
            // 
            this.lblProducts.AutoSize = true;
            this.lblProducts.Location = new System.Drawing.Point(12, 134);
            this.lblProducts.Name = "lblProducts";
            this.lblProducts.Size = new System.Drawing.Size(52, 13);
            this.lblProducts.TabIndex = 8;
            this.lblProducts.Text = "Products:";
            // 
            // txtProduct1
            // 
            this.txtProduct1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct1.Location = new System.Drawing.Point(134, 131);
            this.txtProduct1.Name = "txtProduct1";
            this.txtProduct1.Size = new System.Drawing.Size(293, 20);
            this.txtProduct1.TabIndex = 4;
            this.txtProduct1.Text = "FRIDGE FILTERS";
            this.txtProduct1.TextChanged += new System.EventHandler(this.txtProduct1_TextChanged);
            // 
            // txtProduct2
            // 
            this.txtProduct2.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct2.Location = new System.Drawing.Point(134, 157);
            this.txtProduct2.Name = "txtProduct2";
            this.txtProduct2.Size = new System.Drawing.Size(293, 20);
            this.txtProduct2.TabIndex = 6;
            this.txtProduct2.Text = "HUMIDIFIER FILTERS";
            this.txtProduct2.TextChanged += new System.EventHandler(this.txtProduct2_TextChanged);
            // 
            // txtProduct4
            // 
            this.txtProduct4.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct4.Location = new System.Drawing.Point(133, 209);
            this.txtProduct4.Name = "txtProduct4";
            this.txtProduct4.Size = new System.Drawing.Size(293, 20);
            this.txtProduct4.TabIndex = 10;
            this.txtProduct4.Text = "AIR CLEANERS";
            this.txtProduct4.TextChanged += new System.EventHandler(this.txtProduct4_TextChanged);
            // 
            // txtProduct3
            // 
            this.txtProduct3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct3.Location = new System.Drawing.Point(134, 183);
            this.txtProduct3.Name = "txtProduct3";
            this.txtProduct3.Size = new System.Drawing.Size(293, 20);
            this.txtProduct3.TabIndex = 8;
            this.txtProduct3.Text = "POOL & SPA FILTERS";
            this.txtProduct3.TextChanged += new System.EventHandler(this.txtProduct3_TextChanged);
            // 
            // txtProduct6
            // 
            this.txtProduct6.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct6.Location = new System.Drawing.Point(443, 131);
            this.txtProduct6.Name = "txtProduct6";
            this.txtProduct6.Size = new System.Drawing.Size(293, 20);
            this.txtProduct6.TabIndex = 5;
            this.txtProduct6.Text = "UNDER SINK";
            this.txtProduct6.TextChanged += new System.EventHandler(this.txtProduct6_TextChanged);
            // 
            // txtProduct5
            // 
            this.txtProduct5.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct5.Location = new System.Drawing.Point(134, 235);
            this.txtProduct5.Name = "txtProduct5";
            this.txtProduct5.Size = new System.Drawing.Size(293, 20);
            this.txtProduct5.TabIndex = 12;
            this.txtProduct5.Text = "SEDIMENT FILTERS";
            this.txtProduct5.TextChanged += new System.EventHandler(this.txtProduct5_TextChanged);
            // 
            // txtProduct8
            // 
            this.txtProduct8.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct8.Location = new System.Drawing.Point(443, 183);
            this.txtProduct8.Name = "txtProduct8";
            this.txtProduct8.Size = new System.Drawing.Size(293, 20);
            this.txtProduct8.TabIndex = 9;
            this.txtProduct8.Text = "HUMIDIFIER";
            this.txtProduct8.TextChanged += new System.EventHandler(this.txtProduct8_TextChanged);
            // 
            // txtProduct7
            // 
            this.txtProduct7.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct7.Location = new System.Drawing.Point(443, 157);
            this.txtProduct7.Name = "txtProduct7";
            this.txtProduct7.Size = new System.Drawing.Size(293, 20);
            this.txtProduct7.TabIndex = 7;
            this.txtProduct7.Text = "WHOLE HOUSE";
            this.txtProduct7.TextChanged += new System.EventHandler(this.txtProduct7_TextChanged);
            // 
            // txtProduct9
            // 
            this.txtProduct9.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct9.Location = new System.Drawing.Point(443, 209);
            this.txtProduct9.Name = "txtProduct9";
            this.txtProduct9.Size = new System.Drawing.Size(293, 20);
            this.txtProduct9.TabIndex = 11;
            this.txtProduct9.Text = "CLEANING SUPPLIES";
            this.txtProduct9.TextChanged += new System.EventHandler(this.txtProduct9_TextChanged);
            // 
            // txtProduct10
            // 
            this.txtProduct10.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct10.Location = new System.Drawing.Point(443, 235);
            this.txtProduct10.Name = "txtProduct10";
            this.txtProduct10.Size = new System.Drawing.Size(293, 20);
            this.txtProduct10.TabIndex = 13;
            this.txtProduct10.Text = "UV BULBS";
            this.txtProduct10.TextChanged += new System.EventHandler(this.txtProduct10_TextChanged);
            // 
            // txtProduct12
            // 
            this.txtProduct12.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct12.Location = new System.Drawing.Point(133, 287);
            this.txtProduct12.Name = "txtProduct12";
            this.txtProduct12.Size = new System.Drawing.Size(293, 20);
            this.txtProduct12.TabIndex = 16;
            this.txtProduct12.Text = "FURNACE FILTERS";
            this.txtProduct12.TextChanged += new System.EventHandler(this.txtProduct12_TextChanged);
            // 
            // txtProduct11
            // 
            this.txtProduct11.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct11.Location = new System.Drawing.Point(134, 261);
            this.txtProduct11.Name = "txtProduct11";
            this.txtProduct11.Size = new System.Drawing.Size(293, 20);
            this.txtProduct11.TabIndex = 14;
            this.txtProduct11.Text = "AC FILTERS";
            this.txtProduct11.TextChanged += new System.EventHandler(this.txtProduct11_TextChanged);
            // 
            // txtProduct14
            // 
            this.txtProduct14.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct14.Location = new System.Drawing.Point(443, 287);
            this.txtProduct14.Name = "txtProduct14";
            this.txtProduct14.Size = new System.Drawing.Size(293, 20);
            this.txtProduct14.TabIndex = 17;
            this.txtProduct14.TextChanged += new System.EventHandler(this.txtProduct14_TextChanged);
            // 
            // txtProduct13
            // 
            this.txtProduct13.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProduct13.Location = new System.Drawing.Point(443, 261);
            this.txtProduct13.Name = "txtProduct13";
            this.txtProduct13.Size = new System.Drawing.Size(293, 20);
            this.txtProduct13.TabIndex = 15;
            this.txtProduct13.Text = "CARBON FILTERS";
            this.txtProduct13.TextChanged += new System.EventHandler(this.txtProduct13_TextChanged);
            // 
            // lblPromoCode
            // 
            this.lblPromoCode.AutoSize = true;
            this.lblPromoCode.Location = new System.Drawing.Point(12, 325);
            this.lblPromoCode.Name = "lblPromoCode";
            this.lblPromoCode.Size = new System.Drawing.Size(68, 13);
            this.lblPromoCode.TabIndex = 23;
            this.lblPromoCode.Text = "Promo Code:";
            // 
            // txtPromoCode
            // 
            this.txtPromoCode.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtPromoCode.Location = new System.Drawing.Point(134, 322);
            this.txtPromoCode.Name = "txtPromoCode";
            this.txtPromoCode.Size = new System.Drawing.Size(293, 20);
            this.txtPromoCode.TabIndex = 18;
            this.txtPromoCode.Text = "RETURN20";
            this.txtPromoCode.TextChanged += new System.EventHandler(this.txtPromoCode_TextChanged);
            // 
            // lblWebsite
            // 
            this.lblWebsite.AutoSize = true;
            this.lblWebsite.Location = new System.Drawing.Point(12, 362);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(49, 13);
            this.lblWebsite.TabIndex = 25;
            this.lblWebsite.Text = "Website:";
            // 
            // txtWebsite
            // 
            this.txtWebsite.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWebsite.Location = new System.Drawing.Point(134, 359);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(293, 20);
            this.txtWebsite.TabIndex = 19;
            this.txtWebsite.Text = "DiscountFilters.com";
            this.txtWebsite.TextChanged += new System.EventHandler(this.txtWebsite_TextChanged);
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Location = new System.Drawing.Point(12, 400);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(78, 13);
            this.lblProductName.TabIndex = 27;
            this.lblProductName.Text = "Product Name:";
            // 
            // txtProductName
            // 
            this.txtProductName.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProductName.Location = new System.Drawing.Point(134, 397);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(293, 20);
            this.txtProductName.TabIndex = 20;
            this.txtProductName.Text = "WATER FILTER";
            this.txtProductName.TextChanged += new System.EventHandler(this.txtProductName_TextChanged);
            // 
            // lblProductDescription
            // 
            this.lblProductDescription.AutoSize = true;
            this.lblProductDescription.Location = new System.Drawing.Point(12, 436);
            this.lblProductDescription.Name = "lblProductDescription";
            this.lblProductDescription.Size = new System.Drawing.Size(103, 13);
            this.lblProductDescription.TabIndex = 29;
            this.lblProductDescription.Text = "Product Description:";
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProductDescription.Location = new System.Drawing.Point(134, 433);
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(293, 20);
            this.txtProductDescription.TabIndex = 21;
            this.txtProductDescription.Text = "High quality water filter.";
            this.txtProductDescription.TextChanged += new System.EventHandler(this.txtProductDescription_TextChanged);
            // 
            // lblQRCodeAddress
            // 
            this.lblQRCodeAddress.AutoSize = true;
            this.lblQRCodeAddress.Location = new System.Drawing.Point(12, 478);
            this.lblQRCodeAddress.Name = "lblQRCodeAddress";
            this.lblQRCodeAddress.Size = new System.Drawing.Size(95, 13);
            this.lblQRCodeAddress.TabIndex = 31;
            this.lblQRCodeAddress.Text = "QR Code Address:";
            // 
            // txtQRCodeAddress
            // 
            this.txtQRCodeAddress.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtQRCodeAddress.Location = new System.Drawing.Point(134, 475);
            this.txtQRCodeAddress.Name = "txtQRCodeAddress";
            this.txtQRCodeAddress.Size = new System.Drawing.Size(293, 20);
            this.txtQRCodeAddress.TabIndex = 22;
            this.txtQRCodeAddress.Text = "www.discountfilters.com/PS01-12345";
            this.txtQRCodeAddress.TextChanged += new System.EventHandler(this.txtQRCodeAddress_TextChanged);
            // 
            // lblReorderInstructions
            // 
            this.lblReorderInstructions.AutoSize = true;
            this.lblReorderInstructions.Location = new System.Drawing.Point(12, 551);
            this.lblReorderInstructions.Name = "lblReorderInstructions";
            this.lblReorderInstructions.Size = new System.Drawing.Size(105, 13);
            this.lblReorderInstructions.TabIndex = 33;
            this.lblReorderInstructions.Text = "Reorder Instructions:";
            // 
            // txtReorderInstructions
            // 
            this.txtReorderInstructions.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtReorderInstructions.Location = new System.Drawing.Point(134, 548);
            this.txtReorderInstructions.Multiline = true;
            this.txtReorderInstructions.Name = "txtReorderInstructions";
            this.txtReorderInstructions.Size = new System.Drawing.Size(293, 69);
            this.txtReorderInstructions.TabIndex = 24;
            this.txtReorderInstructions.Text = "For more information, reordering or installation instructions, visit us at www.Di" +
    "scountFitlers.com or scan the code.";
            this.txtReorderInstructions.TextChanged += new System.EventHandler(this.txtReorderInstructions_TextChanged);
            // 
            // lblQRCodeInstructions
            // 
            this.lblQRCodeInstructions.AutoSize = true;
            this.lblQRCodeInstructions.Location = new System.Drawing.Point(12, 513);
            this.lblQRCodeInstructions.Name = "lblQRCodeInstructions";
            this.lblQRCodeInstructions.Size = new System.Drawing.Size(111, 13);
            this.lblQRCodeInstructions.TabIndex = 35;
            this.lblQRCodeInstructions.Text = "QR Code Instructions:";
            // 
            // txtQRCodeInstructions
            // 
            this.txtQRCodeInstructions.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtQRCodeInstructions.Location = new System.Drawing.Point(134, 510);
            this.txtQRCodeInstructions.Name = "txtQRCodeInstructions";
            this.txtQRCodeInstructions.Size = new System.Drawing.Size(293, 20);
            this.txtQRCodeInstructions.TabIndex = 23;
            this.txtQRCodeInstructions.Text = "Scan to reorder";
            this.txtQRCodeInstructions.TextChanged += new System.EventHandler(this.txtQRCodeInstructions_TextChanged);
            // 
            // chkContinuous
            // 
            this.chkContinuous.AutoSize = true;
            this.chkContinuous.Checked = true;
            this.chkContinuous.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkContinuous.Location = new System.Drawing.Point(133, 636);
            this.chkContinuous.Name = "chkContinuous";
            this.chkContinuous.Size = new System.Drawing.Size(15, 14);
            this.chkContinuous.TabIndex = 25;
            this.chkContinuous.UseVisualStyleBackColor = true;
            this.chkContinuous.CheckedChanged += new System.EventHandler(this.chkContinuous_CheckedChanged);
            // 
            // lblContinuous
            // 
            this.lblContinuous.AutoSize = true;
            this.lblContinuous.Location = new System.Drawing.Point(9, 637);
            this.lblContinuous.Name = "lblContinuous";
            this.lblContinuous.Size = new System.Drawing.Size(114, 13);
            this.lblContinuous.TabIndex = 38;
            this.lblContinuous.Text = "Use continuous labels:";
            // 
            // lblLabelLength
            // 
            this.lblLabelLength.AutoSize = true;
            this.lblLabelLength.Location = new System.Drawing.Point(11, 669);
            this.lblLabelLength.Name = "lblLabelLength";
            this.lblLabelLength.Size = new System.Drawing.Size(112, 13);
            this.lblLabelLength.TabIndex = 40;
            this.lblLabelLength.Text = "Label Length (inches):";
            // 
            // txtLabelLength
            // 
            this.txtLabelLength.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtLabelLength.Location = new System.Drawing.Point(133, 666);
            this.txtLabelLength.Name = "txtLabelLength";
            this.txtLabelLength.Size = new System.Drawing.Size(293, 20);
            this.txtLabelLength.TabIndex = 26;
            this.txtLabelLength.Text = "12.25";
            this.txtLabelLength.TextChanged += new System.EventHandler(this.txtLabelLength_TextChanged);
            // 
            // chkUseDefaults
            // 
            this.chkUseDefaults.AutoSize = true;
            this.chkUseDefaults.Checked = true;
            this.chkUseDefaults.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUseDefaults.Enabled = false;
            this.chkUseDefaults.Location = new System.Drawing.Point(598, 712);
            this.chkUseDefaults.Name = "chkUseDefaults";
            this.chkUseDefaults.Size = new System.Drawing.Size(87, 17);
            this.chkUseDefaults.TabIndex = 41;
            this.chkUseDefaults.Text = "Use Defaults";
            this.chkUseDefaults.UseVisualStyleBackColor = true;
            this.chkUseDefaults.CheckedChanged += new System.EventHandler(this.chkUseDefaults_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(131, 689);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "* include full label length plus 1/8\" per label for borders";
            // 
            // frmPromoLabels
            // 
            this.AcceptButton = this.btnPrint;
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PapayaWhip;
            this.ClientSize = new System.Drawing.Size(800, 741);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkUseDefaults);
            this.Controls.Add(this.lblLabelLength);
            this.Controls.Add(this.txtLabelLength);
            this.Controls.Add(this.lblContinuous);
            this.Controls.Add(this.chkContinuous);
            this.Controls.Add(this.lblQRCodeInstructions);
            this.Controls.Add(this.txtQRCodeInstructions);
            this.Controls.Add(this.lblReorderInstructions);
            this.Controls.Add(this.txtReorderInstructions);
            this.Controls.Add(this.lblQRCodeAddress);
            this.Controls.Add(this.txtQRCodeAddress);
            this.Controls.Add(this.lblProductDescription);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.lblProductName);
            this.Controls.Add(this.txtProductName);
            this.Controls.Add(this.lblWebsite);
            this.Controls.Add(this.txtWebsite);
            this.Controls.Add(this.lblPromoCode);
            this.Controls.Add(this.txtPromoCode);
            this.Controls.Add(this.txtProduct14);
            this.Controls.Add(this.txtProduct13);
            this.Controls.Add(this.txtProduct12);
            this.Controls.Add(this.txtProduct11);
            this.Controls.Add(this.txtProduct10);
            this.Controls.Add(this.txtProduct9);
            this.Controls.Add(this.txtProduct8);
            this.Controls.Add(this.txtProduct7);
            this.Controls.Add(this.txtProduct6);
            this.Controls.Add(this.txtProduct5);
            this.Controls.Add(this.txtProduct4);
            this.Controls.Add(this.txtProduct3);
            this.Controls.Add(this.txtProduct2);
            this.Controls.Add(this.lblProducts);
            this.Controls.Add(this.txtProduct1);
            this.Controls.Add(this.lblSecondaryPromoText);
            this.Controls.Add(this.txtSecondaryPromoText);
            this.Controls.Add(this.lblPercentOff);
            this.Controls.Add(this.txtPercentOff);
            this.Controls.Add(this.lblOpeningPromoText);
            this.Controls.Add(this.txtOpeningPromo);
            this.Controls.Add(this.btnPrint);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPromoLabels";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Print Promo Labels";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox txtOpeningPromo;
        private System.Windows.Forms.Label lblOpeningPromoText;
        private System.Windows.Forms.Label lblPercentOff;
        private System.Windows.Forms.TextBox txtPercentOff;
        private System.Windows.Forms.Label lblSecondaryPromoText;
        private System.Windows.Forms.TextBox txtSecondaryPromoText;
        private System.Windows.Forms.Label lblProducts;
        private System.Windows.Forms.TextBox txtProduct1;
        private System.Windows.Forms.TextBox txtProduct2;
        private System.Windows.Forms.TextBox txtProduct4;
        private System.Windows.Forms.TextBox txtProduct3;
        private System.Windows.Forms.TextBox txtProduct6;
        private System.Windows.Forms.TextBox txtProduct5;
        private System.Windows.Forms.TextBox txtProduct8;
        private System.Windows.Forms.TextBox txtProduct7;
        private System.Windows.Forms.TextBox txtProduct9;
        private System.Windows.Forms.TextBox txtProduct10;
        private System.Windows.Forms.TextBox txtProduct12;
        private System.Windows.Forms.TextBox txtProduct11;
        private System.Windows.Forms.TextBox txtProduct14;
        private System.Windows.Forms.TextBox txtProduct13;
        private System.Windows.Forms.Label lblPromoCode;
        private System.Windows.Forms.TextBox txtPromoCode;
        private System.Windows.Forms.Label lblWebsite;
        private System.Windows.Forms.TextBox txtWebsite;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Label lblProductDescription;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label lblQRCodeAddress;
        private System.Windows.Forms.TextBox txtQRCodeAddress;
        private System.Windows.Forms.Label lblReorderInstructions;
        private System.Windows.Forms.TextBox txtReorderInstructions;
        private System.Windows.Forms.Label lblQRCodeInstructions;
        private System.Windows.Forms.TextBox txtQRCodeInstructions;
        private System.Windows.Forms.CheckBox chkContinuous;
        private System.Windows.Forms.Label lblContinuous;
        private System.Windows.Forms.Label lblLabelLength;
        private System.Windows.Forms.TextBox txtLabelLength;
        private System.Windows.Forms.CheckBox chkUseDefaults;
        private System.Windows.Forms.Label label1;
    }
}

